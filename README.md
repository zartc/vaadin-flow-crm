# Spring Boot and Vaadin course source code

**Modified** version of the [Building Modern Web Applications With Spring Boot and Vaadin](https://vaadin.com/docs/latest/flow/tutorials/in-depth-course).

The original Live demo is available here: https://crm.demo.vaadin.com  
*This is not the same version as the one in this repository.*  
Note: login = `user`, passwd = `userpass`

## Text tutorial
You can find a text version of the tutorial in the [Vaadin Documentation](https://vaadin.com/docs/latest/flow/tutorials/in-depth-course).

## Branches

- The `v23` branch is the upstream branch from which the master and develop branches have been derived.
Update it with `git fetch upstream`
- `master` and `develop` are the WIP branches specific for this repository. Don't forget to rebase on
`v23` whenever new updates are fetched from upstream.

## Changes
- Update dependencies
  - java 17
  - Spring-boot 2.7.1


- Introduce `lombok`, `junit-jupiter-params` and `assertj`  
- Integrate `JGitver` with maven to manage automatic version number.

## Future changes
- Switch to Hexagonal Architecture.
- Create Lit component for each panels.
